use std::fs::File;
use std::fs::OpenOptions;
use std::io;
use std::io::prelude::*;
use std::io::{BufRead, BufReader};

//DEBUG SWITCH
const TOGGLE_DEBUG: bool = true;
const PATH_RESA: &str = "resa.txt";

#[derive(Debug)]
struct Room {
    room_name: String,
}

#[derive(Debug)]
struct Reservation {
    resa_id: u32,
    resa_room: String,
    resa_start_time: u32,
    resa_end_time: u32,
    resa_day: u32,
}

impl Room {
    fn create(name: &str) -> Room {
        Room {
            room_name: name.to_string(),
        }
    }
}

impl Reservation {
    fn create(id: u32) -> Reservation {
        Reservation {
            resa_id: id,
            resa_room: String::from("Test Resa"),
            resa_start_time: 0,
            resa_end_time: 0,
            resa_day: 0,
        }
    }
}

fn main() {
    println!(
        "Welcome to Room OS v0.0.2
This program runs basic shell interaction around room structure creation."
    );

    //Load some basic data
    let path_rooms = String::from("rooms.txt");

    // we need a template because we later build rooms by referring to it
    let mut room_template = Room {
        room_name: String::from("Dummy Room"),
    };

    //Building a reservation template as well just to start -- should DELETE later
    /*
    let mut resa_template = Reservation {
        resa_id: 0,
        resa_room: String::from::("template"),
        resa_start_time: 10,
        resa_end_time: 12,
        resa_day: 8,
    };*/

    //Init Room vector
    let mut main_room_vec: &mut Vec<Room> = &mut Vec::new();
    main_room_vec.push(room_template);

    //Init Resa vector
    let mut main_resa_vec: &mut Vec<Reservation> = &mut Vec::new();

    let mut room_file = OpenOptions::new()
        .read(true)
        .create(true)
        .append(true)
        .open(&path_rooms)
        .unwrap();

    loop {
        print_main_menu();
        println!("User input :");

        let mut user_input = String::new();

        io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read line. Program crashed.");

        //Parse data
        user_input = user_input.trim().to_string();

        //EXIT PROMPT
        if user_input == String::from("exit") {
            break;
        }

        //MENU PROMPT
        if user_input == String::from("menu") || user_input == String::from("help") {
            print_main_menu();
            continue;
        }

        //ROOM MENU
        if user_input == String::from("1") {
            loop {
                print_room_menu();
                println!("User input :");

                let mut sub_input = String::new();

                io::stdin()
                    .read_line(&mut sub_input)
                    .expect("Failed to read line. Program crashed.");

                //Parse data
                sub_input = sub_input.trim().to_string();

                //ROOM CREATION
                if sub_input == String::from("1") {
                    println!("\nCreate a Room");
                    println!("What is the name of the room :");

                    let mut user_room_name = String::new();

                    io::stdin()
                        .read_line(&mut user_room_name)
                        .expect("Failed to read name input. Program crashed.");

                    user_room_name = user_room_name.trim().to_string();

                    //Test if the name already exists
                    // THIS DOES NOT WORK YET
                    /*
                    for (i, &item) in &main_room_vec.iter().enumerate() {
                        let a: String = item.room_name;
                        let tester: bool = false;

                        tester = check_name_existing(&a, &user_room_name);

                        if tester == true {
                            println!("This room already exists !");
                        } else {
                            println!("Tested against {} but test was negative", a);
                        }
                    }
                    */

                    let user_room = Room::create(&user_room_name);

                    println!("\nRoom created with name : '{}'\n", user_room.room_name);

                    if let Err(e) = writeln!(room_file, "{:?}", user_room) {
                        eprintln!("Couldn't write to file : {}", e);
                    }
                    continue;
                }
                //2-Read Vector
                if sub_input == String::from("2") {
                    println!("Reading main room vector :");

                    let mut count = 0;
                    while count < main_room_vec.len() {
                        println!("{:?}", main_room_vec[count]);
                        count += 1;
                    }
                    continue;
                }

                //RESET FILE
                //sync OK
                if sub_input == String::from("3") {
                    println!("File reinitialized.\n");
                    init_file(&path_rooms);
                    continue;
                }

                //READ FILE
                //sync OK
                if sub_input == String::from("4") {
                    println!("The file contains :");
                    read_to_string(&path_rooms);
                    continue;
                }

                //WRITE SOMETHING TO FILE
                if sub_input == String::from("5") {
                    sync_room_vector(&path_rooms, &mut main_room_vec);
                    continue;
                }

                //CALL THE COMPARE TWO STRINGS FUNCTION
                // MOVE TO A TOOL SUB-MENU
                if sub_input == String::from("6") {
                    let a = "string1";
                    let b = "string2";
                    let mut marker: bool = false;
                    marker = check_name_existing(&a, &b);
                    println!("Marker of calling function is : {}", marker);
                    continue;
                }
                if sub_input == String::from("7") {
                    break;
                } else {
                    //FOR NOW : DO NOTHING
                    println!("\nErr: Unrecognized choice. Please choose a valid option.");
                    continue;
                    //Analyze user input, convert to number. Tolerate faulty input.
                    //let analyzed_input: u32 = match user_input.trim().parse() {
                    //    Ok(num) => num,
                    //    Err(_) => continue,
                };
            }
        }

        if user_input == String::from("2") {
            reservation_menu(&main_resa_vec);
        }
        if user_input == String::from("3") {
            break;
        } else {
            //FOR NOW : DO NOTHING
            //println!("\nUnrecognized choice. Please choose a valid option. Type 'menu' if needed.");
            println!("Err: Unrecognized choice. Please choose a valid option.");
            continue;
            //Analyze user input, convert to number. Tolerate faulty input.
            //let analyzed_input: u32 = match user_input.trim().parse() {
            //    Ok(num) => num,
            //    Err(_) => continue,
        };
    }
}

// MENU DISPLAYS
fn print_main_menu() {
    println!(
        "\nMain Menu :
1.Enter Room sub menu
2.Enter Reservation sub menu
3.EXIT PROGRAM\n"
    );
}

fn print_room_menu() {
    println!(
        "\nRoom Menu :
 1.Create a room.
 2.Read main vector.
 3.Reset the Room file.
 4.Read & print the Room file.
 5.Scan file and sync vector.
 6.Test check_name_existing fn
 7.Return to main menu.\n"
    );
}

fn print_reservation_menu() {
    println!(
        "\nReservation Menu :
  1.Create a Reservation. [NOT IMPLEMENTED]
  2.Read reservation vector.
  3.Reset the Reservation file.
  4.Read & print the Resa file.
  5.Scan file & sync vector. [NOT IMPLEMENTED]
  6.Test check_name_existing function [NOT IMPLEMENTED]
  7.Return to main menu.\n"
    );
}

//RESERVATION SUB-MENU
fn reservation_menu(input_vec: &Vec<Reservation>) {
    let mut resa_file = OpenOptions::new()
        .read(true)
        .create(true)
        .append(true)
        .open(PATH_RESA)
        .unwrap();

    loop {
        print_reservation_menu();
        println!("User input :");

        let mut user_input = String::new();

        io::stdin()
            .read_line(&mut user_input)
            .expect("Failed to read line. Program crashed.");

        //Parse data
        user_input = user_input.trim().to_string();

        //1-Create Reservation
        if user_input == String::from("1") {
            println!("Not implemented yet !");
            continue;
        }

        //2-Read vector
        if user_input == String::from("2") {
            println!("Reading main room vector :");

            let mut count: usize = 0;
            while &count < &input_vec.len() {
                println!("{:?}", &input_vec[count]);
                count += 1;
            }
            continue;
        }

        //3-Reset Resa file
        if user_input == String::from("3") {
            println!("File reinitialized.\n");
            init_file(PATH_RESA);
            continue;
        }

        //4-Read file
        if user_input == String::from("4") {
            read_to_string(PATH_RESA);
            continue;
        }

        //5-Scan file & sync vector
        if user_input == String::from("5") {
            println!("Not implemented yet !");
            continue;
        }

        //6-Test check_name_existing fn
        if user_input == String::from("6") {
            println!("Not implemented yet !");
            continue;
        }

        //7-Exit
        if user_input == String::from("7") {
            break;
        } else {
            //FOR NOW : DO NOTHING
            println!("\nErr: Unrecognized choice. Please choose a valid option.");
            continue;
        }
    }
}

//FILE FUNCTIONS
//Reinitialize file
fn init_file(the_file: &str) -> std::io::Result<()> {
    let mut file = File::create(the_file)?;
    file.write_all(b"")?;
    Ok(())
}

//Display file contents
fn read_to_string(s: &str) -> io::Result<()> {
    let mut f = File::open(s)?;
    let mut buffer = String::new();

    f.read_to_string(&mut buffer)?;
    println!("File '{}' contains :", s);
    println!("{}\n", buffer);
    Ok(())
}

//ROOM VECTOR FUNCTIONS --> This should be implemented on Room struct not a seperate function
//This function scans the room file and syncs it's content to the vector it is given as input
fn sync_room_vector<'a>(the_file: &String, input_vec: &'a mut Vec<Room>) -> &'a mut Vec<Room> {
    if TOGGLE_DEBUG {
        println!(
            "Function sync_room_vector properly called. File : '{}' was opened",
            the_file
        );
    }
    let f = File::open(the_file).unwrap();
    let reader = BufReader::new(f);

    //Flush the main vector before use
    input_vec.clear();

    //Read the file line by line using the lines() iterator from std::io::BufRead
    for (index, line) in reader.lines().enumerate() {
        //Ignore errors.
        let line = line.unwrap();
        let room_name_from_field = first_field(&line);
        input_vec.push(Room::create(&room_name_from_field));
    }
    if TOGGLE_DEBUG {
        println!("Vector inside :");
        for (i, item) in input_vec.iter().enumerate() {
            println!("index : {} - item : {:?}", i, item);
        }
    }

    input_vec
}

//This function scans for two "" and returns a slice of the string in between
//It is used to fetch the first field of a struct
fn first_field(s: &String) -> &str {
    let bytes = s.as_bytes();

    let mut start: usize = 0;
    let mut end: usize = 0;

    for (i, &item) in bytes.iter().enumerate() {
        if end == 0 {
            if item == b'"' {
                //find the first "
                if start == 0 {
                    start = i + 1;
                }
                //find the second "
                else {
                    end = i;
                }
            }
        }
    }
    //Return a slice of what's in between
    &s[start..end]
}

//This function tests two references to a string byte per byte.
//Returns a boolean, true if there is a difference, false if there is no difference.
fn check_name_existing(a: &str, b: &str) -> bool {
    let bytes_a = a.as_bytes();
    let bytes_b = b.as_bytes();

    let mut marker: bool = false;

    for (i, &item) in bytes_a.iter().enumerate() {
        println!("index1 : {} - item1 : {:?}", i, item);
        for (j, &item2) in bytes_b.iter().enumerate() {
            if j == i {
                println!("index2 : {} - item2 : {:?}", j, item2);
                if item2 == bytes_a[i] {
                    println!("A match was found !\n");
                } else {
                    marker = true;
                    println!("A mismatch was found ! Marker is {}\n", marker);
                }
            } else {
                continue;
            }
        }
    }
    marker
}
