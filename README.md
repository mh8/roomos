# Room OS

Room OS or Room _Open Sharing / Open Source_ is a small lightweight program I'm creating to deal with room reservations in workplaces.

It's aim is to provide a simple graphic interface for non-tech users to manage timetables for different rooms and feature ways to visualize the data using smart filters.

The goal of this project is for it to be use as much Rust as possible. As it requires a GUI, and given the current case and debate around GUI in Rust (https://areweguiyet.com/newsfeed/2019-01-13_rust2019.html) it is evident that some part of the code on the repository might not be Rust.

Development will prioritize keeping that amount as small as possible.

### Stage of Development

At the moment, the program is an ultra basic shell interactive program.

Currently under development is some basic file management to implement the use of persistent data.

### Currently working on :

- Repair 'exit' prompt from room sub menu to correctly bring back to main menu --> This will be naturally solved when room menu body will be in it's own function and out of main().
- Finish sync_room_vector function -> Exclude the possibility to create two rooms using the same name > Parse the vector for the names & compare
- Add a cursive-based TUI interface ?
Ressources : https://github.com/gyscos/cursive/blob/master/doc/tutorial_1.md

- Start implementing a simple reservation system, without handling real dates/times just dummy input


### Later Milestones

- Implement date handling via external crates
- Implement GUI via external crates _(Research required)_ Azul seems promising. Full rust approach and draws using WebRenderer
